#PCA steps
* data matrix **D**n constructed from vertical traces as follows: First row contains samples from n1 to n2 of east component, second row contains samples from n1 to n2 of north compoenent
* subtract the mean of each variable from the dataset to center the data around the origin
    * = subtract average of all samples between n1 and n2 from every component of **D**n
    * results in matrix **A**n
* ~~compute covariance matrix~~
    * How do I do that?
* Calculate **A** * **A**^T
* ~~calculate eigenvalues and corresponding eigenvectors of the covariance matrix~~
* singular value decomposition of **A** * **A**^T (numpy.linalg.svd) returns **u**, **s**, **v**
    * **s** diagonal matrix containing eigenvalues in descending order
    * **u** square matrix containing eigenvectors correpsonding to the eigenvalues in **s**. First principal component is the first row in **u**
    * **v** = **u**^T
* ~~normalize each orthogonal eigenvectors~~
* ~~covariance matrix is transformed to a diagonal matrix with diagonal elements representing the variance of each axis~~
* ~~proportion of variance that each eigenvector represents can be calculated by dividing the eigenvalue corresponding to that eigenvector by the sum of all eigenvalues~~
* resolving 180° ambiguity
    * calculate weighted average direction H
    * apply flipping condition (B4)
* calculate uncertainty in degrees
    * calculate the angle between first principal component and the added vector from the first and second principal component
    * scale the uncertainty to 45°, where 45° represents an uncertainty of 100%



# Interfaces

## Inputs
* ZNE data/first motion amplitude for single events in a certain time window around the P wave arrival and/or SH waves
    * p wave: time of first detection until first meaningful extremum in vertical component
        * n1, float: p wave onset in vertical component
        * n2, float: first maximum (10*SNR) after n1

## Outputs
* back azimuth pdf for single event

# The function backAzimuthPrincipalComponentAnalysis
* identify P phases with P and Pmax in event
    * save phases to list
    * if there is no P onset with P and Pmax in event return empty pdf list
* loop over P phases and do data preparation/prepare input
    * copy trace
    * detrend
    * filter (zerophase = True)
    * taper
    * cut out from P to Pmax
    * save all event traces in dictionary with phase as key, trace as value
* Singular value decomposition
    * loop over every event in dict
    * create NP array from stream snippet
    * use np.linalg.svd on array*array^T
* Calculate flipping condition
* calculate backazimuth from flipped eigenvektor
    * estimate uncertainty by length relationship between eigenvectors
* create pdf with uncertainty and append it to pdf list
* return pdf list
