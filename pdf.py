#!/usr/bin/env python

class Pdf(object):
    def __init__(self, parameter, phase, data):
        """
        Create pdf for parameter, phase
        :param parameter: Which parameter to create pdf for
        :type parameter: str
        :param phase: which phase to create pdf for
        :type phase: str
        :param data: 2D array containing (index, pdf value)
        :type data: numpy.ndarray
        """
        import numpy as np
        
        parameters = ['time', 'back-azimuth', 'distance']
        assert parameter in parameters, 'parameter must be "time", "back-azimuth" or "distance"'
        self.parameter = parameter
        
        assert isinstance(phase, str), 'phase must be str'
        self.phase = phase
        
        assert isinstance(data, np.ndarray), 'phase must be numpy array'
        assert len(data) == 2, 'data must be a two-dimensional numpy array (degree, pdf)'
        self.data = data 
