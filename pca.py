"""
Function to calculate single station back azimuth using principal component
analysis. See backAzimuthPCA documentation for info on usage.
"""

import sys
import numpy as np
import obspy
from obspy.core.event import read_events
from pdf import Pdf

# define all P phases this method can be used for
allowed_phases = set(("Pg,Pb,Pn,PmP,"
                      "P,PP,PPP,PcP,Pdif,Pdiff,SP,SSP,SPP,ScP,"
                      "PKP,PKPab,PKPbc,PKPdf,PKPdif,P'P',PKKP,SKP").split(','))


def backAzimuthPCA(st, evt, binsize=1., filter_freq=0.08, resample_freq=10.):
    """
    Function to calculate pdfs for backazimuth from P phases in event using
    principal component analysis
    :param st: Full waveform data stream
    :type st: obspy.core.stream.Stream
    :param evt: Event object containing picks with phase name, onset time
    and max time
    :type evt: obspy.core.event.event.Event
    :param binsize: Bin size of pdf in degrees
    :type binsize: float
    :param filter_freq: Frequency for high pass filter
    :type filter_freq: float
    :param resample_freq: Frequency for resampling wf data, improves results but
    takes longer to calculate
    :type resample_freq: float
    :return: list containing backazimut pdfs
    :rtype: list
    """
    picks = getValidPicks(evt)
    print("Event contains {} valid picks for pca".format(len(picks)))
    pdflist = []
    # prepare stream
    print("Preparing stream")
    st_prep = prep(st, filter_freq, resample_freq)
    for phasename in picks:
        start = picks[phasename]['start']
        stop = picks[phasename]['stop']
        st_trimmed = st_prep.slice(start, stop)
        print("Length of trimmed data: {} samples".format(len(st_trimmed)))
        picks[phasename]["stream"] = st_trimmed
        print("SVD")
        eigenvectors, eigenvalues = svd(st_trimmed)
        print("Fliping condition")
        if flipping_condition(st, eigenvectors[0]):
            eigenvectors = flip_eigenvektors(eigenvectors)
        backaz = calc_backazimuth(eigenvectors[0])
        uncertainty = estimate_uncertainty(eigenvectors, eigenvalues)
        print("Creating pdf")
        PDF = calculatePdfFromUncertainty(phasename, backaz, uncertainty, binsize)
        picks[phasename]["pdf"] = PDF
        pdflist.append(PDF)

    return pdflist


def getValidPicks(event):
    """
    Loop over all picks in an event and return the P picks where a P and Pmax
    pick is available as a dict
    :type event: obspy.core.event
    :return: Dictionary containing valid picks such as
    {'P' = {'start'=UTCDateTime, 'end'=UTCDateTime}}
    Empty if no valid picks were found
    :rtype: dict
    """

    available_picks = {}
    valid_picks = {}
    # Put all available picks in event into a dict
    for pick in event.picks:
        available_picks[pick.phase_hint] = pick

    # look at picks that have max attribute
    for phasename, pick in available_picks.items():
        if phasename.endswith('max') and phasename[:-3] in allowed_phases and phasename[:-3] in available_picks:
            # pick has P and Pmax pick, append it to valid_picks
            n1 = available_picks[pick.phase_hint[:-3]].time
            n2 = pick.time
            valid_picks[phasename[:-3]] = {'start': n1, 'stop': n2}
    return valid_picks


def calculatePdfFromUncertainty(phase, expectation, deviation, binsize=1.):
    """
    Create gaussian PDF from best estimate and uncertainty
    :param phase: which phase the back-azimuth was calculated for
    :type phase: str
    :param expectation: mean or expectation of value
    :type expectation: float
    :param deviation: standard deviation of value
    :type deviation: float
    :param binsize: size of bins in degrees
    :type binsize: float
    :return: Pdf object for given parameters
    :rtype: Pdf
    """
    # convert rad to deg for calculation
    expectation = expectation * 180/np.pi
    deviation = deviation * 180/np.pi
    print("Creating PDF for {}: exp: {}, dev: {}".format(phase, expectation, deviation))
    degrees = np.arange(0., 360., binsize)
    pdf_list=[]
    for degree in degrees:
        # calculate normal distribution values
        zahler = np.exp(-((degree - expectation) ** 2 / (2 * deviation ** 2)))
        nenner = np.sqrt(2 * np.pi * deviation ** 2)
        pdf_list.append(zahler/nenner)
    d = np.vstack((degrees, pdf_list))
    return Pdf(phase=phase, parameter='back-azimuth', data=d)


def prep(stream, filter_freq, res_freq=None):
    """
    Copy ,detrend,taper, filter and resample stream
    :param stream: waveform data stream
    :type stream: obspy.core.stream.Stream
    :param filter_freq: Cutoff frequency for the highpass filter
    :type filter_freq: float
    :param res_freq: Target frequency for resampling. Resampling is only done
    if res_freq is provided
    :type res_freq: float
    :return: copy of stream after data processing
    :rtype: obspy.core.stream.Stream
    """
    st = stream.copy()
    if res_freq:
        print("Resampling")
        st.resample(res_freq)
    st.detrend('linear')
    st.taper(0.05, type='cosine')
    st.filter(type='highpass', freq=filter_freq, zerophase=True)

    return st


def svd(stream):
    """
    Calculates eigenvectors U and eigenvalues s for east and north component
    :param stream: stream of event cut from n1 to n2
    :type stream: obspy.core.Stream
    :return: tuple containing U eigenvectors in columns, s eigenvalues is a
    diagonal matrix containing the eigenvalues arranged in descending order
    :rtype: tuple
    """
    east = stream.select(component="E")
    north = stream.select(component="N")
    a = np.vstack((east, north))
    U, s, V = np.linalg.svd(a, full_matrices=True)
    return U, s


def flipping_condition(st, eigenvector):
    """
    Implement flipping condition to eliminate 180° ambiguity as described by
    equation B1 in "Real-Time Back Azimuth for Earthquake Early Warning -
    Eisermann et. al."
    :param st: stream containing ZNE traces
    :type st: obspy.core.Stream.stream
    :param eigenvector:
    :type eigenvector: np.ndarray
    :return: true if backazimuth has to be flipped, false otherwise
    :rtype: bool
    """
    AmplitudeE = st.select(component="E")[0].data
    AmplitudeN = st.select(component="N")[0].data
    AmplitudeZ = st.select(component="Z")[0].data
    N = st.select(component="Z")[0].stats.npts

    # implement helper functions to keep code cleaner
    def h(z):
        """
        Main flipping decision depending on sign of Z component
        """
        return 1 if z > 0 else -1

    H = 0
    for n in range(N-1):
        an = np.array(( (AmplitudeE[n+1] - AmplitudeE[n]),
                        (AmplitudeN[n+1] - AmplitudeN[n]) ))
        an_norm = normalize(an)

        a = np.cross(an_norm, eigenvector)
        b = h(AmplitudeZ[n])
        c = np.linalg.norm(an)

        H += a * b * c/N * c

    return False if H > 0 else True


def flip_eigenvektors(eigenvektors):
    return -1 * eigenvektors


def calc_backazimuth(eigenvector):
    """
    Calculate backazimuth angle clockwise against north direction (0, 1)
    :return: backazimuth angle in radians
    :rtype: float
    """
    angle = angle_between(eigenvector, (0, 1))
    # this formula gives the smallest angle to north, to get the correct
    # geological angle add 180° if pointing West
    if eigenvector[0] < 0:
        return angle + np.pi
    else:
        return angle


def estimate_uncertainty(eigenvectors, eigenvalues):
    """
    Estimate uncertainty of backazimuth value
    :param eigenvectors:
    :type eigenvectors: np.ndarray
    :param eigenvalues:
    :type eigenvalues: np.ndarray
    :return: uncertainty as angle in rad
    :rtype: float
    """
    u1 = eigenvectors[0] * eigenvalues[0]
    u2 = eigenvectors[1] * eigenvalues[1]
    beta = angle_between(u1, u1+u2)

    max_beta=1/4*np.pi
    if np.absolute(beta) < max_beta:
        beta=(beta/max_beta)*2*np.pi
    else:
        beta=2*np.pi
    return beta


def normalize(vec):
    """
    Return unit vector

    >>> normalize(np.array((1, 1)))
    array([ 0.70710678,  0.70710678])

    :param vec: vector to normalze
    :type vec: np.ndarray
    :return: unit vector
    :rtype: np.ndarray
    """
    return vec/np.linalg.norm(vec)

def angle_between(vector1, vector2):
    """
    Calculate smallest angle between two vectors
    :type vector1: array
    :type vector2: array
    :return: angle in radians
    :rtype: float
    """
    return np.arccos(np.dot(vector1, vector2) / (np.linalg.norm(vector1) * np.linalg.norm(vector2)))

def main():
    """
    call subfunctions here
    """

    evt_catalog_filename = 'test_events.xml'
    # this is just for development, final release of pca function will get a single
    # event from catalog
    cat = read_events(pathname_or_url=evt_catalog_filename, format='QUAKEML')

    st = obspy.read("insight_test.mseed")
    st = st.select(channel='MH*')  # only use seismometer data
    b = backAzimuthPCA(st, cat[0])[0]

    import matplotlib.pyplot as plt
    plt.plot(b.data[0], b.data[1])
    plt.show()

if __name__ == '__main__':
    sys.exit(main())
